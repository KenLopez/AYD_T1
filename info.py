from flask import Flask, request

app = Flask(__name__)

@app.route("/info", methods=['GET'])
def info():
    return {
        'info': 'Kenneth López - 201906570'
    }

if __name__ == '__main__':
      app.run(host='0.0.0.0', port=4000, debug=True)