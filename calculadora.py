from flask import Flask, request

app = Flask(__name__)

@app.route("/suma", methods=['POST'])
def suma():
    data = request.get_json()
    return {
        'result': data['op1'] + data['op2']
    }

@app.route("/resta", methods=['POST'])
def resta():
    data = request.get_json()
    return {
        'result': data['op1'] - data['op2']
    }

@app.route("/multiplicacion", methods=['POST'])
def multiplicacion():
    data = request.get_json()
    return {
        'result': data['op1'] * data['op2']
    }

if __name__ == '__main__':
      app.run(port=3000, debug=True)